use open_api_generator::actix_web_generation::ActixGenerator;

use openapi::OpenApi;

fn main() {
    let path = std::env::args().nth(1).unwrap();
    let spec = match openapi::from_path(path).unwrap() {
        OpenApi::V2(_) => unimplemented!(),
        OpenApi::V3_0(x) => x,
    };

    let stdout = std::io::stdout();

    dbg!(&spec.paths);

    let gen = ActixGenerator::new(stdout, &spec);
    let _w = gen.gen_spec().unwrap();
}
