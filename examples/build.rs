use open_api_generator::{actix_web_generation::ActixGenerator, openapi, OpenApi};

use std::fs;

fn main() {
    let spec = match openapi::from_path("schemas/api.yaml").unwrap() {
        OpenApi::V2(_) => unimplemented!(),
        OpenApi::V3_0(x) => x,
    };

    let out_file = fs::File::create("schemas/api.out.rs").unwrap();
    let gen = ActixGenerator::new(out_file, &spec);
    let _ = gen.gen_spec().unwrap();

    let out_file = fs::File::create("schemas/common.out.rs").unwrap();
    let common = match openapi::from_path("schemas/common.yaml").unwrap() {
        OpenApi::V2(_) => unimplemented!(),
        OpenApi::V3_0(x) => x,
    };
    let gen = ActixGenerator::new(out_file, &common);
    let _ = gen.gen_spec().unwrap();
}
